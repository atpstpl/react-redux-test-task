import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router'

import * as actions from 'App/stores/resources/actions'
import { getEntities } from 'App/stores/resources'

import AddList from './components/AddList'
import ListView from './components/ListView'

const TodoLists = ({ lists, addlist, openList, router }) => {
	return(
		<div>
			<section className='pa3 pa5-ns'>
				
				<AddList onSubmit={({list}, _, {reset}) => {
	        		addlist(list)
	        		reset()
	      		}} />
				<ListView {...{lists, openList, router}}/>

			</section>
		</div>
	)
}

export default connect(
  state => ({
    lists: getEntities('lists')(state)
  }),
  dispatch => ({
    addlist: (name) => dispatch(actions.submitEntity({ name }, {type: 'lists'})),
  })
)(TodoLists)