import React, { PropTypes } from 'react'

import ListComponent from '../ListComponent'

const ListView = ({ lists, openList, router }) => {
	
	return(
		<ul className='list pl0 ml0 center mw6 ba b--light-silver br2'>
          {lists
            ? lists.map((list, i) =>
              <ListComponent
                key={i}
                {...list}
                openList={() => router.push({pathname:'/todos',query:{'id':list.id}})}
              />
            )
            : <p className='ph3 pv3 tc'>No List found</p>
          }
		</ul>
	)
}

ListView.propTypes = {
  todos: PropTypes.array
}

export default ListView