import React, { PropTypes } from 'react'
import classNames from 'classnames'

const ListComponent = ({ name, openList }) => {
  return (
    <li className='ph3 pv3 pointer bg-animate hover-bg-light-gray' onClick={() => openList()}>{name}</li>
  )
}

ListComponent.propTypes = {
  name: PropTypes.string.isRequired,
}

export default ListComponent