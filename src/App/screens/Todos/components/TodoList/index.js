import React, { PropTypes } from 'react'

import Todo from '../Todo'

const sortByDate = (arr) => arr.sort((a, b) => {
  // Turn your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
  return new Date(b.createdAt) - new Date(a.createdAt)
})

class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter : 'all'
    }
    this.fitlerTodos = this.fitlerTodos.bind(this)
  }
  
  fitlerTodos(e){
    this.setState({
      filter : e.target.value
    });
  }

  render(){
    
    const { listTodos, toggleTodo} = this.props
    const sortedTodos = listTodos && listTodos[0] ? sortByDate(listTodos) : [];
    const filteredTodos = this.state.filter === 'all' ? sortedTodos :
      this.state.filter === 'completed' ? sortedTodos.filter(todo => todo.completed === true) : sortedTodos.filter(todo => todo.completed === false)
    return (
      <div>
        <div className = 'pl0 ml0 center mw6' >
          <input type="radio" name="todoType" onChange={this.fitlerTodos} checked = {this.state.filter === 'all'} value="all"/> All 
          <input type="radio" name="todoType" onChange={this.fitlerTodos} checked = {this.state.filter === 'completed'} value="completed"/> Completed
          <input type="radio" name="todoType" onChange={this.fitlerTodos} checked = {this.state.filter === 'pending'} value="pending"/> Pending
        </div>
        <ul className='list pl0 ml0 center mw6 ba b--light-silver br2'>
          {filteredTodos
            ? filteredTodos.map((todo, i) =>
              <Todo
                key={i}
                {...todo}
                toggle={() => toggleTodo(todo, !todo.completed)}
                isLast={(filteredTodos.length - 1) === i}
              />
            )
            : <p className='ph3 pv3 tc'>No todos found</p>
          }
        </ul>
      </div>
    )
  }
}

TodoList.propTypes = {
  todos: PropTypes.array
}

export default TodoList
