import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import * as actions from 'App/stores/resources/actions'
import { getEntities, getEntity } from 'App/stores/resources'

import AddTodo from './components/AddTodo'
import TodoList from './components/TodoList'

class Todos extends React.Component {  
  render(){
      const { todos, addTodo, toggleTodo, location, list } = this.props
      const listTodos = todos ? todos.filter(todo => todo.listID === list.id) : []
      return (
        <section className='pa3 pa5-ns'>
          <AddTodo onSubmit={({todo}, _, {reset}) => {
            addTodo(todo,list.id)
            reset()
          }} />

          <h1 className='f4 bold center mw6'>All Todos</h1>

          <TodoList {...{ listTodos, toggleTodo }} />
        </section>
      )
  }
}

Todos.propTypes = {
  todos: PropTypes.array,
  list: PropTypes.object
}

export default connect(
  (state, ownProps) => ({
    todos: getEntities('todos')(state),
    list : getEntity('lists', ownProps.location.query.id)(state)
  }),
  dispatch => ({
    addTodo: (text,listID) => dispatch(actions.submitEntity({ text, listID }, {type: 'todos'})),
    toggleTodo: (todo, completed) => dispatch(actions.updateEntity({ ...todo, completed }, {type: 'todos'}))
  })
)(Todos)
